Set-ExecutionPolicy RemoteSigned -scope CurrentUser

$MagicKey="d13ref0un133f"

if ($MagicKey -ne $args[0]) {
    Write-Output "`r`nYou can't run installer.ps1 dirrectly! Use start_installer.bat`r`n"
    Exit
}

$CurrDir=split-path -parent $MyInvocation.MyCommand.Definition

try{
    Start-Process 'powershell' -ArgumentList "-File $CurrDir\scoop.ps1 -ExecutionPolicy Bypass" -Wait -NoNewWindow
    Start-Process 'powershell' -ArgumentList "-ExecutionPolicy Bypass -File $CurrDir\nodejs.ps1" -Wait -NoNewWindow
    Start-Process 'powershell' -ArgumentList "-File $CurrDir\xpack.ps1 -ExecutionPolicy Bypass" -Wait -NoNewWindow
    Start-Process 'powershell' -ArgumentList "-File $CurrDir\gnuarm.ps1 -ExecutionPolicy Bypass" -Wait -NoNewWindow
    Start-Process 'powershell' -ArgumentList "-File $CurrDir\chocorm.ps1 -ExecutionPolicy Bypass" -Wait -NoNewWindow

    Write-Output "`r`nScript finished!`r`n"
}
catch {
    Write-Output "`r`nError!`r`n"
}
