# Скрипт установщик утилит для GNU MCU Eclipse Plugin

## Описание

Данный скрипт устанавливает Build Tools и OpenOCD таким образом, чтобы GNU MCU Eclipce Plugin мог их правильно детектировать и зпускать.

## Инструкция

1. Откройте PowerShell от имени администратора (Пуск → Windows PowerShell, ПКМ → Выполнить от имени администратора)
2. Выполните команду:
    ```
        Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
    ```
3. При необходимости введите Y и нажмите Enter.
4. Выполните командуЖ
    ```
        Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    ```
5. Запустите start_installer.bat, дождитесь окончания работы скрипта.

## Пример вывода скрипта

```
    Initializing...
    Downloading...
    Extracting...
    Creating shim...
    Adding ~\scoop\shims to your path.
    Scoop was installed successfully!
    Type 'scoop help' for instructions.
    Refreshing environment variables from registry for cmd.exe. Please wait...Finished..
    C:\ProgramData\scoop\persist\nodejs\bin\xpm -> C:\ProgramData\scoop\persist\nodejs\bin\node_modules\xpm\bin\xpm.js
    + xpm@0.4.5
    added 305 packages from 146 contributors in 20.871s
    Refreshing environment variables from registry for cmd.exe. Please wait...Finished..
    xPack manager - install package(s)
    (node:8356) ExperimentalWarning: The fs.promises API is experimental

    Processing @gnu-mcu-eclipse/windows-build-tools@2.11.1-1...
    Installing globally in 'C:\Users\Дмитрий Лисин\AppData\Roaming\xPacks\@gnu-mcu-eclipse\windows-build-tools\2.11.1-1'...
    Extracting 'gnu-mcu-eclipse-build-tools-2.11-20180428-1604-win64.zip'...
    42 files extracted.

    'xpm install' completed in 1.868 sec.
    Refreshing environment variables from registry for cmd.exe. Please wait...Finished..
    xPack manager - install package(s)
    (node:8384) ExperimentalWarning: The fs.promises API is experimental

    Processing @gnu-mcu-eclipse/openocd@0.10.0-8.1...
    Installing globally in 'C:\Users\Дмитрий Лисин\AppData\Roaming\xPacks\@gnu-mcu-eclipse\openocd\0.10.0-8.1'...
    Extracting 'gnu-mcu-eclipse-openocd-0.10.0-8-20180512-1921-win64.zip'...
    787 files extracted.

    'xpm install' completed in 3.066 sec.

    Script finished!
```

# Изменения

## [1.0.0] - 09.07.2018
### Добавлено
- README.md с описанием инструкций по запуску
- bat-файл для запуска скрипта установки
- ps-скрипт для последовательной установки пакетных менеджеров и актуальных версий windows-build-tools, openocd